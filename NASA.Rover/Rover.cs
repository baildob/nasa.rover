﻿using System;

namespace NASA.Rover
{
    public class Rover
    {
        public Rover() { }

        private const char COMMAND_LEFT = 'L';
        private const char COMMAND_RIGHT = 'R';
        private const char COMMAND_MOVE = 'M';

        public Position CurrentPosition { get; private set; }

        public void Explore(Plateu plateu, string startingPosition, string commands)
        {
            CurrentPosition = Position.Parse(startingPosition);
            Execute(commands, plateu); 
        }

        private void Execute(string commands, Plateu plateu)
        {
            CharEnumerator enumerator = commands.GetEnumerator(); 
            while (enumerator.MoveNext())
            {
                char command = enumerator.Current; 
                Execute(command, plateu); 
            }
        }

        private void Execute(char command, Plateu plateu)
        {
            switch (command)
            {
                case COMMAND_LEFT:
                    TurnLeft();
                    break;
                case COMMAND_RIGHT:
                    TurnRight();
                    break; 
                case COMMAND_MOVE:
                    MoveForward(plateu);
                    break; 
            }
        }

        private void TurnLeft()
        {
            switch (CurrentPosition.Direction)
            {
                case Direction.NORTH:
                    CurrentPosition.Direction = Direction.WEST;
                    break; 
                case Direction.EAST:
                    CurrentPosition.Direction = Direction.NORTH;
                    break; 
                case Direction.SOUTH:
                    CurrentPosition.Direction = Direction.EAST;
                    break; 
                case Direction.WEST:
                    CurrentPosition.Direction = Direction.SOUTH;
                    break; 
            }
        }

        private void TurnRight()
        {
            switch (CurrentPosition.Direction)
            {
                case Direction.NORTH:
                    CurrentPosition.Direction = Direction.EAST;
                    break; 
                case Direction.EAST:
                    CurrentPosition.Direction = Direction.SOUTH;
                    break; 
                case Direction.SOUTH:
                    CurrentPosition.Direction = Direction.WEST;
                    break; 
                case Direction.WEST:
                    CurrentPosition.Direction = Direction.NORTH;
                    break; 
            }
        }

        private void MoveForward(Plateu plateu)
        {
            Position nextPosition = GetNextPosition();

            if (plateu.IsValid(nextPosition))
            {
                CurrentPosition = nextPosition;
            }
            else
            {
                throw new SystemException(string.Format("Unable to execute move to {0}", nextPosition.ToString())); 
            }
        }

        private Position GetNextPosition()
        {
            Position position = new Position();
            position.Direction = CurrentPosition.Direction;

            if (position.Direction == Direction.NORTH)
            {
                position.X = CurrentPosition.X;
                position.Y = CurrentPosition.Y += 1;
            }

            if (position.Direction == Direction.EAST)
            {
                position.X = CurrentPosition.X += 1;
                position.Y = CurrentPosition.Y; 
            }

            if (position.Direction == Direction.SOUTH)
            {
                position.X = CurrentPosition.X; 
                position.Y = CurrentPosition.Y -= 1;
            }

            if (position.Direction == Direction.WEST)
            {
                position.X = CurrentPosition.X -= 1;
                position.Y = CurrentPosition.Y; 
            }

            return position; 
        }
    }
}
