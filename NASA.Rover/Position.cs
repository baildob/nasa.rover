﻿using System;

namespace NASA.Rover
{
    public class Position
    {
        public int X { get; set; }
        public int Y { get; set; }
        public char Direction { get; set; }

        public static Position Parse(string input)
        {
            input = input.Replace(" ", "");
            
            Position position = new Position();
            position.X = Convert.ToInt32(input.Substring(0, 1));
            position.Y = Convert.ToInt32(input.Substring(1, 1));
            position.Direction = Convert.ToChar(input.Substring(2, 1)); 

            return position; 
        }

        public override string ToString()
        {
            return string.Format("{0} {1} {2}", X, Y, Direction);
        }
    }
}
