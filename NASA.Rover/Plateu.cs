﻿namespace NASA.Rover
{
    public class Plateu
    {
        public int X1 { get; private set; }
        public int Y1 { get; private set; }
        public int X2 { get; private set; }
        public int Y2 { get; private set; }

        public Plateu(int x2, int y2)
        {
            X1 = 0;
            Y1 = 0;
            X2 = x2;
            Y2 = y2;
        }

        public bool IsValid(Position position)
        {
            if (position.X >= X1 && position.X <= X2 && position.Y >= Y1 && position.Y <= Y2)
                return true;
            else
                return false;  
        }
    }
}
