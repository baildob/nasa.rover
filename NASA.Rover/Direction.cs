﻿namespace NASA.Rover
{
    public class Direction
    {
        public const char NORTH = 'N';
        public const char EAST = 'E';
        public const char SOUTH = 'S';
        public const char WEST = 'W';
    }
}
