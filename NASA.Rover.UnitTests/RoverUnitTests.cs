﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace NASA.Rover.UnitTests
{
    [TestClass]
    public class RoverUnitTests
    {
        [TestMethod]
        public void TestRover1()
        {
            Plateu plateu = new Plateu(5, 5);

            Rover rover = new Rover();
            rover.Explore(plateu, "1 2 N", "LMLMLMLMM");
            string currentPosition = rover.CurrentPosition.ToString();

            Assert.IsTrue(currentPosition == "1 3 N");
        }

        [TestMethod]
        public void TestRover2()
        {
            Plateu plateu = new Plateu(5, 5);

            Rover rover = new Rover();
            rover.Explore(plateu, "3 3 E", "MMRMMRMRRM");
            string currentPosition = rover.CurrentPosition.ToString();

            Assert.IsTrue(currentPosition == "5 1 E");
        }
    }
}
